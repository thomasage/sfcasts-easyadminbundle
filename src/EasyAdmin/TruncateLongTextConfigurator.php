<?php

declare(strict_types=1);

namespace App\EasyAdmin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldConfiguratorInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use function Symfony\Component\String\u;

final class TruncateLongTextConfigurator implements FieldConfiguratorInterface
{
    private const MAX_LENGTH = 25;

    public function configure(FieldDto $field, EntityDto $entityDto, AdminContext $context): void
    {
        if (strlen((string) $field->getFormattedValue()) <= self::MAX_LENGTH) {
            return;
        }
        $crud = $context->getCrud();
        if ($crud && Crud::PAGE_DETAIL === $crud->getCurrentPage()) {
            return;
        }
        $truncatedValue = u($field->getFormattedValue())->truncate(self::MAX_LENGTH, '...', false);
        $field->setFormattedValue($truncatedValue);
    }

    public function supports(FieldDto $field, EntityDto $entityDto): bool
    {
        return TextareaField::class === $field->getFieldFqcn();
    }
}
