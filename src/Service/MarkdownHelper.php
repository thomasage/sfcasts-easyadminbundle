<?php

declare(strict_types=1);

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;

final class MarkdownHelper
{
    public function __construct(
        private readonly MarkdownParserInterface $markdownParser,
        private readonly CacheInterface $cache,
        private readonly bool $isDebug,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function parse(string $source): string
    {
        if (false !== stripos($source, 'cat')) {
            $this->logger->info('Meow!');
        }

        if ($this->isDebug) {
            return $this->markdownParser->transformMarkdown($source);
        }

        return (string) $this->cache->get('markdown_'.md5($source), fn () => $this->markdownParser->transformMarkdown($source));
    }
}
