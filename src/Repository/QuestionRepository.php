<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Question>
 */
final class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class);
    }

    /**
     * @return Question[]
     */
    public function findLatest(): array
    {
        return $this->createQueryBuilder('question')
            ->orderBy('question.createdAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Question[]
     */
    public function findTopVoted(): array
    {
        return $this->createQueryBuilder('question')
            ->orderBy('question.votes', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Question[] Returns an array of Question objects
     */
    public function findAllApprovedOrderedByNewest(): array
    {
        return $this->addIsApprovedQueryBuilder()
            ->orderBy('q.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    private function addIsApprovedQueryBuilder(QueryBuilder $qb = null): QueryBuilder
    {
        return $this->getOrCreateQueryBuilder($qb)
            ->andWhere('q.isApproved = :approved')
            ->setParameter('approved', true);
    }

    private function getOrCreateQueryBuilder(QueryBuilder $qb = null): QueryBuilder
    {
        return $qb ?: $this->createQueryBuilder('q');
    }
}
