<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Question;
use App\Repository\QuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class QuestionController extends AbstractController
{
    public function __construct(private readonly LoggerInterface $logger, private readonly bool $isDebug)
    {
    }

    #[Route('/', name: 'app_homepage')]
    public function homepage(QuestionRepository $repository): Response
    {
        $questions = $repository->findAllApprovedOrderedByNewest();

        return $this->render('question/homepage.html.twig', [
            'questions' => $questions,
        ]);
    }

    #[Route('/questions/new')]
    public function new(): Response
    {
        return new Response('Sounds like a GREAT feature for V2!');
    }

    #[Route('/questions/{slug}', name: 'app_question_show')]
    public function show(Question $question): Response
    {
        if (!$question->getIsApproved()) {
            throw $this->createNotFoundException(sprintf('Question %s has not been approved yet', $question->getId()));
        }

        if ($this->isDebug) {
            $this->logger->info('We are in debug mode!');
        }

        return $this->render('question/show.html.twig', [
            'question' => $question,
        ]);
    }

    #[Route('/questions/{slug}/vote', name: 'app_question_vote', methods: 'POST')]
    public function questionVote(Question $question, Request $request, EntityManagerInterface $entityManager): Response
    {
        $direction = $request->request->get('direction');

        if ('up' === $direction) {
            $question->upVote();
        } elseif ('down' === $direction) {
            $question->downVote();
        }

        $entityManager->flush();

        return $this->redirectToRoute('app_question_show', [
            'slug' => $question->getSlug(),
        ]);
    }
}
