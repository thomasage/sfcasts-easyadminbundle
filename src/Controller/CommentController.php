<?php

declare(strict_types=1);

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class CommentController extends AbstractController
{
    #[Route('/comments/{id}/vote/{direction<up|down>}', methods: 'POST')]
    public function commentVote($id, string $direction, LoggerInterface $logger): JsonResponse
    {
        // todo - use id to query the database

        // use real logic here to save this to the database
        if ('up' === $direction) {
            $logger->info('Voting up!');
            $currentVoteCount = random_int(7, 100);
        } else {
            $logger->info('Voting down!');
            $currentVoteCount = random_int(0, 5);
        }

        return $this->json(['votes' => $currentVoteCount]);
    }
}
