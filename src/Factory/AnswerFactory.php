<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\User;
use App\Repository\AnswerRepository;
use DateTimeInterface;
use Exception;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @method static       Answer|Proxy createOne(array $attributes = [])
 * @method static       Answer[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static       Answer|Proxy find($criteria)
 * @method static       Answer|Proxy findOrCreate(array $attributes)
 * @method static       Answer|Proxy first(string $sortedField = 'id')
 * @method static       Answer|Proxy last(string $sortedField = 'id')
 * @method static       Answer|Proxy random(array $attributes = [])
 * @method static       Answer|Proxy randomOrCreate(array $attributes = [])
 * @method static       Answer[]|Proxy[] all()
 * @method static       Answer[]|Proxy[] findBy(array $attributes)
 * @method static       Answer[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static       Answer[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static       AnswerRepository|RepositoryProxy repository()
 * @method Answer|Proxy create($attributes = [])
 * @extends ModelFactory<Answer>
 */
final class AnswerFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return Answer::class;
    }

    /**
     * @return array{answer:string,question:Question|Proxy<Question>,createdAt:DateTimeInterface,answeredBy:User|Proxy<User>,votes:int}
     *
     * @throws Exception
     */
    protected function getDefaults(): array
    {
        return [
            'answer' => self::faker()->sentence(),
            'question' => QuestionFactory::random(),
            'createdAt' => self::faker()->dateTimeBetween('-100 days', '-1 minute'),
            'answeredBy' => UserFactory::random(),
            'votes' => random_int(-5, 10),
        ];
    }
}
