<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Question;
use App\Entity\Topic;
use App\Entity\User;
use App\Repository\QuestionRepository;
use DateTimeInterface;
use Exception;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @method static             Question|Proxy findOrCreate(array $attributes)
 * @method static             Question|Proxy random()
 * @method static             Question[]|Proxy[] randomSet(int $number)
 * @method static             Question[]|Proxy[] randomRange(int $min, int $max)
 * @method static             QuestionRepository|RepositoryProxy repository()
 * @method Question|Proxy     create($attributes = [])
 * @method Question[]|Proxy[] createMany(int $number, $attributes = [])
 * @extends ModelFactory<Question>
 */
final class QuestionFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return Question::class;
    }

    public function unpublished(): self
    {
        return $this->addState(['isApproved' => false]);
    }

    /**
     * @return array{name:string,question:string,createdAt:DateTimeInterface,askedBy:User|Proxy<User>,votes:int,topic:Topic|Proxy<Topic>,isApproved:bool}
     *
     * @throws Exception
     */
    protected function getDefaults(): array
    {
        /** @var string $question */
        $question = self::faker()->paragraphs(self::faker()->numberBetween(1, 4), true);

        return [
            'name' => self::faker()->realText(50),
            'question' => $question,
            'createdAt' => self::faker()->dateTimeBetween('-100 days', '-1 minute'),
            'askedBy' => UserFactory::random(),
            'votes' => random_int(-20, 50),
            'topic' => TopicFactory::random(),
            'isApproved' => true,
        ];
    }
}
