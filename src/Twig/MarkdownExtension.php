<?php

declare(strict_types=1);

namespace App\Twig;

use App\Service\MarkdownHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class MarkdownExtension extends AbstractExtension
{
    public function __construct(private readonly MarkdownHelper $markdownHelper)
    {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('parse_markdown', [$this, 'parseMarkdown'], ['is_safe' => ['html']]),
        ];
    }

    public function parseMarkdown(string $value): string
    {
        return $this->markdownHelper->parse($value);
    }
}
